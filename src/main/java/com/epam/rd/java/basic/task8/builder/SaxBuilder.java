package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.container.Laptops;
import com.epam.rd.java.basic.task8.entity.Laptop;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;
import static com.epam.rd.java.basic.task8.tag.LaptopTags.*;

public class SaxBuilder extends AbstractXMLBuilder{
    private static final Logger LOGGER = Logger.getLogger(SaxBuilder.class.getName());
    private XMLOutputFactory factory;

    public SaxBuilder(Laptops laptops,String outputXmlFile) {
        super(laptops,outputXmlFile);
        factory = XMLOutputFactory.newInstance();
    }

    @Override
    public void buildXml() {
        try {
            XMLStreamWriter writer = factory.createXMLStreamWriter(new FileOutputStream(outputXmlFile));
            writer.writeStartDocument("UTF-8", "1.0");
            writer.writeStartElement(LAPTOPS.getValue());
            writer.writeAttribute("xmlns:ns","http://www.w3.org/2001/XMLSchema-instance");
            writer.writeAttribute("xmlns","http://laptops.com/laptops");
            writer.writeAttribute("ns:schemaLocation","http://laptops.com/laptops input.xsd");
            for (Laptop laptop : laptops) {
                writer.writeStartElement(LAPTOP.getValue());
                makeNode(writer, laptop.getBrand(), BRAND.getValue());
                makeNode(writer, laptop.getModel(), MODEL.getValue());
                makeNode(writer, laptop.getCountry(), COUNTRY.getValue());
                makeNode(writer, String.valueOf(laptop.getPrice()), PRICE.getValue());
                writer.writeStartElement(VISUALPARAMETERS.getValue());
                makeNode(writer, laptop.getVisualParameters().getColor(), COLOR.getValue());
                makeNode(writer, laptop.getVisualParameters().getType(), TYPE.getValue());
                makeNode(writer, Integer.toString(laptop.getVisualParameters().getDiagonal()), DIAGONAL.getValue());
                writer.writeEndElement();
                writer.writeStartElement(PARAMETERS.getValue());
                makeNode(writer, laptop.getParameters().getCpu(), CPU.getValue());
                makeNode(writer, laptop.getParameters().getGpu(), GPU.getValue());
                makeNode(writer, Integer.toString(laptop.getParameters().getRam()), RAM.getValue());
                writer.writeEndElement();

                writer.writeEndElement();
            }
            writer.writeEndElement();
            writer.writeEndDocument();
            writer.flush();
        }catch (XMLStreamException | IOException e){
            LOGGER.severe(e.getMessage());
        }
    }

    private void makeNode(XMLStreamWriter w, String value, String tag) throws XMLStreamException {
        w.writeStartElement(tag);
        w.writeCharacters(value);
        w.writeEndElement();
    }
}
