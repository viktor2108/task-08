package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.container.Laptops;
import com.epam.rd.java.basic.task8.entity.Laptop;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Logger;
import static com.epam.rd.java.basic.task8.tag.LaptopTags.*;

public class StaxBuilder extends AbstractXMLBuilder{

    private static final Logger LOGGER = Logger.getLogger(StaxBuilder.class.getName());
    private XMLOutputFactory factory;

    public StaxBuilder(Laptops laptops,String outputXmlFile) {
        super(laptops,outputXmlFile);
        factory = XMLOutputFactory.newInstance();
    }
    @Override
    public void buildXml() {
        try {
            XMLEventWriter writer = factory.createXMLEventWriter(new FileOutputStream(outputXmlFile));
            XMLEventFactory eventFactory = XMLEventFactory.newInstance();
            StartDocument doc = eventFactory.createStartDocument("UTF-8", "1.0");
            writer.add(doc);
            Attribute attribute1 = eventFactory.createAttribute("xmlns:ns", "http://www.w3.org/2001/XMLSchema-instance");
            Attribute attribute2 = eventFactory.createAttribute("xmlns", "http://laptops.com/laptops");
            Attribute attribute3 = eventFactory.createAttribute("ns:schemaLocation", "http://laptops.com/laptops input.xsd");
            StartElement startElement = eventFactory.createStartElement("", "",LAPTOPS.getValue());
            writer.add(startElement);
            writer.add(attribute1);
            writer.add(attribute2);
            writer.add(attribute3);
            for (Laptop laptop : laptops) {
                makeLaptop(laptop,eventFactory,writer);
            }
            EndElement endElement = eventFactory.createEndElement("", "", LAPTOPS.getValue());
            writer.add(endElement);
            EndDocument endDoc = eventFactory.createEndDocument();
            writer.add(endDoc);
        } catch (XMLStreamException | FileNotFoundException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private void makeLaptop(Laptop laptop, XMLEventFactory factory, XMLEventWriter writer) throws XMLStreamException {
        StartElement laptopStart = factory.createStartElement("", "", LAPTOP.getValue());
        EndElement laptopEnd = factory.createEndElement("", "", LAPTOP.getValue());
        writer.add(laptopStart);
        makeNode(factory, writer, laptop.getBrand(), BRAND.getValue());
        makeNode(factory, writer, laptop.getModel(), MODEL.getValue());
        makeNode(factory, writer, laptop.getCountry(), COUNTRY.getValue());
        makeNode(factory, writer, String.valueOf(laptop.getPrice()), PRICE.getValue());
        StartElement vp = factory.createStartElement("", "", VISUALPARAMETERS.getValue());
        writer.add(vp);
        makeNode(factory, writer, laptop.getVisualParameters().getColor(), COLOR.getValue());
        makeNode(factory, writer, laptop.getVisualParameters().getType(), TYPE.getValue());
        makeNode(factory, writer, Integer.toString(laptop.getVisualParameters().getDiagonal()), DIAGONAL.getValue());
        EndElement vpEnd = factory.createEndElement("", "", VISUALPARAMETERS.getValue());
        writer.add(vpEnd);
        StartElement param = factory.createStartElement("", "", PARAMETERS.getValue());
        writer.add(param);
        makeNode(factory, writer, laptop.getParameters().getCpu(), CPU.getValue());
        makeNode(factory, writer, laptop.getParameters().getGpu(), GPU.getValue());
        makeNode(factory, writer, Integer.toString(laptop.getParameters().getRam()), RAM.getValue());
        EndElement paramEnd = factory.createEndElement("", "", PARAMETERS.getValue());
        writer.add(paramEnd);

        writer.add(laptopEnd);
    }

    private void makeNode(XMLEventFactory factory, XMLEventWriter w, String value, String tag) throws XMLStreamException {
        StartElement name = factory.createStartElement("", "", tag);
        Characters ch = factory.createCharacters(value);
        EndElement end = factory.createEndElement("", "", tag);
        w.add(name);
        w.add(ch);
        w.add(end);
    }
}
