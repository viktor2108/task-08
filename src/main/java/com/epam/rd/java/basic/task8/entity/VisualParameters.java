package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {

    private String color;
    private String type;
    private Integer diagonal;

    public VisualParameters(){

    }

    public VisualParameters(String color, String type, int diagonal) {
        this.color = color;
        this.type = type;
        this.diagonal = diagonal;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(int diagonal) {
        this.diagonal = diagonal;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "color='" + color + '\'' +
                ", type='" + type + '\'' +
                ", diagonal=" + diagonal +
                '}';
    }
}
