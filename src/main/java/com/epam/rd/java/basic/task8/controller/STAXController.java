package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.container.Laptops;
import com.epam.rd.java.basic.task8.entity.Laptop;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.tag.LaptopTags.*;

/**
 * Controller for StAX parser.
 */
public class STAXController {
	private static final Logger LOGGER = Logger.getLogger(STAXController.class.getName());
	private String xmlFileName;
	private final XMLInputFactory factory;
	private Laptops laptops;
	private XMLEventReader reader;
	private Laptop current;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		laptops = new Laptops();
		factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		factory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);
	}

	public void parse() {
		current = null;
		try {
			FileInputStream file = new FileInputStream(xmlFileName);
			reader = factory.createXMLEventReader(file);

			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					event = makeStart(event, startElement);
				}
				if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					if (LAPTOP.getValue().equals(endElement.getName().getLocalPart())) {
						laptops.add(current);
					}
				}
			}

		} catch (XMLStreamException | FileNotFoundException e){
			LOGGER.severe(e.getMessage());
		}
	}

	private XMLEvent makeStart(XMLEvent event, StartElement startElement) throws XMLStreamException {
		if (LAPTOP.getValue().equals(startElement.getName().getLocalPart())) {
			current = new Laptop();
		} else if (PRICE.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.setPrice(Integer.parseInt(event.asCharacters().getData()));
		} else if (BRAND.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.setBrand(event.asCharacters().getData());
		} else if (MODEL.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.setModel(event.asCharacters().getData());
		} else if (COUNTRY.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.setCountry(event.asCharacters().getData());
		} else if (startElement.isStartElement()) {
			StartElement startElement1 = event.asStartElement();
			buildParameters(startElement1, current);
			buildVisualParameters(startElement1, current);
		}
		return event;
	}

	private void buildVisualParameters(StartElement startElement, Laptop current) throws XMLStreamException {
		XMLEvent event;
		if (COLOR.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.getVisualParameters().setColor(event.asCharacters().getData());
		} else if (TYPE.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.getVisualParameters().setType(event.asCharacters().getData());
		} else if (DIAGONAL.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.getVisualParameters().setDiagonal(Integer.parseInt(event.asCharacters().getData()));
		}
	}

	private void buildParameters(StartElement startElement, Laptop current) throws XMLStreamException {
		XMLEvent event;
		if (CPU.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.getParameters().setCpu(event.asCharacters().getData());
		} else if (GPU.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.getParameters().setGpu(event.asCharacters().getData());
		} else if (RAM.getValue().equals(startElement.getName().getLocalPart())) {
			event = reader.nextEvent();
			current.getParameters().setRam(Integer.parseInt(event.asCharacters().getData()));
		}
	}

	public Laptops getLaptops() {
		return laptops;
	}
}