package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.container.Laptops;
import com.epam.rd.java.basic.task8.entity.Laptop;
import com.epam.rd.java.basic.task8.entity.Parameters;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.logging.Logger;

import static com.epam.rd.java.basic.task8.tag.LaptopTags.*;

/**
 * Controller for DOM parser.
 */
public class DOMController {
	private static final Logger LOGGER = Logger.getLogger(DOMController.class.getName());
	private String xmlFileName;
	private DocumentBuilder documentBuilder;
    private Laptops laptops;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		laptops = new Laptops();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			documentBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			LOGGER.severe(e.getMessage());
		}
	}

	public void parse() {
		Document document;
		try {
			document = documentBuilder.parse(xmlFileName);
			Element root = document.getDocumentElement();
			NodeList nodes = root.getElementsByTagName(LAPTOP.getValue());
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				Laptop laptop = buildLaptop(element);
				laptops.add(laptop);
			}
		} catch (IOException  | SAXException e) {
			LOGGER.severe(e.getMessage());
		}
	}

	private Laptop buildLaptop(Element element) {
		Laptop laptop = new Laptop();
		laptop.setBrand(getTextContent(element,BRAND.getValue()));
		laptop.setModel(getTextContent(element,MODEL.getValue()));
		laptop.setCountry(getTextContent(element,COUNTRY.getValue()));
		laptop.setPrice(Integer.parseInt(getTextContent(element,PRICE.getValue())));
		Element visualParameters = (Element) element.getElementsByTagName(VISUALPARAMETERS.getValue()).item(0);
		laptop.setVisualParameters(buildVisualParameters(visualParameters));
		Element parameters = (Element) element.getElementsByTagName(PARAMETERS.getValue()).item(0);
		laptop.setParameters(buildParameters(parameters));

		return laptop;
	}

	private Parameters buildParameters(Element element) {
		Parameters parameters = new Parameters();
		parameters.setCpu(getTextContent(element,CPU.getValue()));
		parameters.setGpu(getTextContent(element,GPU.getValue()));
		parameters.setRam(Integer.parseInt(getTextContent(element,RAM.getValue())));
		return parameters;
	}

	private VisualParameters buildVisualParameters(Element element) {
		VisualParameters visualParameters = new VisualParameters();
		visualParameters.setColor(getTextContent(element,COLOR.getValue()));
		visualParameters.setType(getTextContent(element,TYPE.getValue()));
		visualParameters.setDiagonal(Integer.parseInt(getTextContent(element,DIAGONAL.getValue())));
		return visualParameters;
	}

	private String getTextContent(Element element, String value) {
		Node node = element.getElementsByTagName(value).item(0);
		return node.getTextContent();
	}

	public Laptops getLaptops() {
		return laptops;
	}
}
