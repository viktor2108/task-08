package com.epam.rd.java.basic.task8.tag;

public enum LaptopTags {
    LAPTOPS("laptops"),
    LAPTOP("laptop"),
    BRAND("brand"),
    MODEL("model"),
    COUNTRY("country"),
    PRICE("price"),
    COLOR("color"),
    TYPE("type"),
    DIAGONAL("diagonal"),
    CPU("cpu"),
    GPU("gpu"),
    RAM("ram"),
    VISUALPARAMETERS("visualParameters"),
    PARAMETERS("parameters");

    private String value;

    LaptopTags(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
