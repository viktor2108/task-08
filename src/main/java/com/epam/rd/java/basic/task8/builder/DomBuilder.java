package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.container.Laptops;
import com.epam.rd.java.basic.task8.entity.Laptop;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Logger;
import static com.epam.rd.java.basic.task8.tag.LaptopTags.*;

public class DomBuilder extends AbstractXMLBuilder{

    private static final Logger LOGGER = Logger.getLogger(DomBuilder.class.getName());

    public DomBuilder(Laptops laptops,String outputXmlFile) {
        super(laptops,outputXmlFile);
    }

    @Override
    public void buildXml() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();
            Element root = doc.createElement(LAPTOPS.getValue());
            root.setAttribute("xmlns:ns","http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xmlns","http://laptops.com/laptops");
            root.setAttribute("ns:schemaLocation","http://laptops.com/laptops input.xsd");
            doc.appendChild(root);
            for (Laptop laptop : laptops) {
                Element entity = doc.createElement(LAPTOP.getValue());
                Element brand = doc.createElement(BRAND.getValue());
                brand.appendChild(doc.createTextNode(laptop.getBrand()));
                Element model = doc.createElement(MODEL.getValue());
                model.appendChild(doc.createTextNode(laptop.getModel()));
                Element country = doc.createElement(COUNTRY.getValue());
                country.setTextContent(laptop.getCountry());
                Element price = doc.createElement(PRICE.getValue());
                price.appendChild(doc.createTextNode(String.valueOf(laptop.getPrice())));
                Element visualParameters = doc.createElement(VISUALPARAMETERS.getValue());
                Element color = doc.createElement(COLOR.getValue());
                color.setTextContent(laptop.getVisualParameters().getColor());
                Element type = doc.createElement(TYPE.getValue());
                type.setTextContent(laptop.getVisualParameters().getType());
                Element diagonal = doc.createElement(DIAGONAL.getValue());
                diagonal.setTextContent(Integer.toString(laptop.getVisualParameters().getDiagonal()));
                Element parameters = doc.createElement(PARAMETERS.getValue());
                Element cpu = doc.createElement(CPU.getValue());
                cpu.setTextContent(laptop.getParameters().getCpu());
                Element gpu = doc.createElement(GPU.getValue());
                gpu.appendChild(doc.createTextNode(laptop.getParameters().getGpu()));
                Element ram = doc.createElement(RAM.getValue());
                ram.setTextContent(Integer.toString(laptop.getParameters().getRam()));

                visualParameters.appendChild(color);
                visualParameters.appendChild(type);
                visualParameters.appendChild(diagonal);

                parameters.appendChild(cpu);
                parameters.appendChild(gpu);
                parameters.appendChild(ram);

                entity.appendChild(brand);
                entity.appendChild(model);
                entity.appendChild(country);
                entity.appendChild(price);
                entity.appendChild(visualParameters);
                entity.appendChild(parameters);

                root.appendChild(entity);

                write(doc);

            }
        } catch (ParserConfigurationException | TransformerException | IOException e){
            LOGGER.severe(e.getMessage());
        }
    }

    private void write(Document doc) throws TransformerException, FileNotFoundException {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        Transformer tr = factory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new FileOutputStream(outputXmlFile));
        tr.transform(source, result);
    }
}
