package com.epam.rd.java.basic.task8.builder;

import com.epam.rd.java.basic.task8.container.Laptops;

public abstract class AbstractXMLBuilder {
    protected Laptops laptops;
    protected String outputXmlFile;

    public AbstractXMLBuilder(Laptops laptops,String outputXmlFile) {
        this.laptops = laptops;
        this.outputXmlFile = outputXmlFile;
    }

    public abstract void buildXml();
}
