package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.builder.DomBuilder;
import com.epam.rd.java.basic.task8.builder.SaxBuilder;
import com.epam.rd.java.basic.task8.builder.StaxBuilder;
import com.epam.rd.java.basic.task8.container.Laptops;
import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Laptop;


import java.util.Comparator;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parse();
		Laptops laptopsDom = domController.getLaptops();

		// sort (case 1)
		laptopsDom.getLaptops().sort(new Comparator<Laptop>() {
			@Override
			public int compare(Laptop o1, Laptop o2) {
				return o1.getPrice().compareTo(o2.getPrice());
			}
		});

		// save
		String outputXmlFile = "output.dom.xml";
		DomBuilder domBuilder = new DomBuilder(laptopsDom,outputXmlFile);
		domBuilder.buildXml();

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse();
		Laptops laptopsSax = saxController.getLaptops();

		// sort  (case 2)
		laptopsSax.getLaptops().sort(new Comparator<Laptop>() {
			@Override
			public int compare(Laptop o1, Laptop o2) {
				return o1.getBrand().compareTo(o2.getBrand());
			}
		});

				// save
		outputXmlFile = "output.sax.xml";
		SaxBuilder saxBuilder = new SaxBuilder(laptopsSax,outputXmlFile);
		saxBuilder.buildXml();
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		Laptops laptopsStax = staxController.getLaptops();
		
		// sort  (case 3)
		laptopsStax.getLaptops().sort(new Comparator<Laptop>() {
			@Override
			public int compare(Laptop o1, Laptop o2) {
				return o2.getVisualParameters().getDiagonal().compareTo(o1.getVisualParameters().getDiagonal());
			}
		});
		
		// save
		outputXmlFile = "output.stax.xml";
		StaxBuilder staxBuilder = new StaxBuilder(laptopsStax,outputXmlFile);
		staxBuilder.buildXml();

	}
}
