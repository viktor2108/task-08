package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.container.Laptops;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Controller for SAX parser.
 */
public class SAXController {
	private static final Logger LOGGER = Logger.getLogger(SAXController.class.getName());
	private String xmlFileName;
	private Laptops laptops;
    private final LaptopHandler handler;
	private XMLReader reader;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		laptops = new Laptops();
		handler = new LaptopHandler();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			SAXParser parser = factory.newSAXParser();
			reader = parser.getXMLReader();
		} catch (SAXException | ParserConfigurationException e) {
			LOGGER.severe(e.getMessage());
		}
		reader.setContentHandler(handler);
	}

    public void parse() {
		try {
			reader.parse(xmlFileName);
		} catch (SAXException | IOException e) {
			LOGGER.severe(e.getMessage());
		}
		laptops = handler.getLaptops();
	}

	public Laptops getLaptops() {
		return laptops;
	}
}