package com.epam.rd.java.basic.task8.container;

import com.epam.rd.java.basic.task8.entity.Laptop;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Laptops implements Iterable<Laptop> {
    private final List<Laptop> container;

    public Laptops() {
        container = new ArrayList<>();
    }

    public Laptops(List<Laptop> container) {
        this.container = container;
    }

    public List<Laptop> getLaptops() {
        return container;
    }

    public void add(Laptop laptop) {
        container.add(laptop);
    }

    public Laptop get(int i) {
        return container.get(i);
    }

    public Iterator<Laptop> iterator() {
        return container.iterator();
    }

    @Override
    public String toString() {
        return container.toString();
    }
}
