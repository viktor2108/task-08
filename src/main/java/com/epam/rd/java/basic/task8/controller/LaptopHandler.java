package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.container.Laptops;
import com.epam.rd.java.basic.task8.entity.Laptop;
import com.epam.rd.java.basic.task8.tag.LaptopTags;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import static com.epam.rd.java.basic.task8.tag.LaptopTags.*;

import java.util.EnumSet;
import java.util.Locale;

public class LaptopHandler extends DefaultHandler {
    private final Laptops laptops;
    private Laptop current;
    private LaptopTags currentTag;
    private final EnumSet<LaptopTags> set;
    private static final String LAPTOP_ELEMENT = LAPTOP.getValue();


    public LaptopHandler() {
        laptops = new Laptops();
        set = EnumSet.range(BRAND,RAM);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals(LAPTOP_ELEMENT)) {
            current = new Laptop();
            return;
        }
        LaptopTags lt = LaptopTags.valueOf(qName.toUpperCase(Locale.ROOT));
        if (set.contains(lt)) {
            currentTag = lt;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String data = new String(ch,start,length);
        if(currentTag != null) {
            switch (currentTag) {
                case BRAND:
                    current.setBrand(data);
                    break;
                case MODEL:
                    current.setModel(data);
                    break;
                case COUNTRY:
                    current.setCountry(data);
                    break;
                case PRICE:
                    current.setPrice(Integer.parseInt(data));
                    break;
                case COLOR:
                    current.getVisualParameters().setColor(data);
                    break;
                case TYPE:
                    current.getVisualParameters().setType(data);
                    break;
                case DIAGONAL:
                    current.getVisualParameters().setDiagonal(Integer.parseInt(data));
                    break;
                case CPU:
                    current.getParameters().setCpu(data);
                    break;
                case GPU:
                    current.getParameters().setGpu(data);
                    break;
                case RAM:
                    current.getParameters().setRam(Integer.parseInt(data));
                    break;
                default:
                    throw new EnumConstantNotPresentException(currentTag.getDeclaringClass(), currentTag.name());
            }
        }
        currentTag = null;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals(LAPTOP_ELEMENT))
            laptops.add(current);
    }

    public Laptops getLaptops(){
        return laptops;
    }
}
