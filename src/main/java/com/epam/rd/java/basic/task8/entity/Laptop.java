package com.epam.rd.java.basic.task8.entity;

import java.util.Objects;

public class Laptop {

    private String brand;
    private String model;
    private String country;
    private Integer price;
    private Parameters parameters = new Parameters();
    private VisualParameters visualParameters = new VisualParameters();

    public Laptop() {

    }

    public Laptop(String brand, String model, String country, int price, Parameters parameters, VisualParameters visualParameters) {
        this.brand = brand;
        this.model = model;
        this.country = country;
        this.price = price;
        this.parameters = parameters;
        this.visualParameters = visualParameters;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Laptop laptop = (Laptop) o;
        return brand.equals(laptop.brand) && model.equals(laptop.model) && country.equals(laptop.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, country);
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", country='" + country + '\'' +
                ", price=" + price +
                ", parameters=" + parameters +
                ", visualParameters=" + visualParameters +
                '}';
    }
}
